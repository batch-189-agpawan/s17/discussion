/*
	You can declare functions by using:
	1. function keyword
	2. function name
	3. open/close parenthesis
	4. open/close curly braces
*/
function sayHello(){
	console.log('Hello there!')
}

// you can invoke a function by calling its function name and including the parenthesis
sayHello()

// you can assign a function to a variable. the function name would not be required.
let sayGoodbye = function(){
	console.log('Goodbye!')
}

// you can invoke a function inside a variable by using the variable name
sayGoodbye()

// You can also re-assign a function as a new value of a variable
sayGoodbye = function(){
	console.log('Au Revoir!')
}

sayGoodbye()

// Declaring a constant variable with a function as a value will not allow that function to be changed or re-assigned
const sayHelloInJapanese = function(){
	console.log('Ohayo!')
}

// sayHelloInJapanese = function(){
// 	console.log("kumusta!")
// }

sayHelloInJapanese()


// GLOBAL scope - you can use a variable inside a function if the variable is declared outside of it.
let action = 'run'

function doSomethingRandom(){
	console.log(action)
}

doSomethingRandom()


// Local/Function scope - you CANNOT use a variable outside of a function if it is within the function scope/curly braces
// function doSomethingRandom(){
// 	let action = 'run'
// }
// 	console.log(action)


// you can nest functions inside of a function as long as you invoke the child function within the sope of the parent function
function viewProduct(){

	console.log('Viewing a product')

	function addToCart(){
		console.log('Added product to cart')

	}
	addToCart()
}

viewProduct()

// Alert function is a bulit-in javascript function where we can show alerts to the user
function singASong(){
	alert('la la la')
}

// Any statement like console.log will run only after the alert has been closed if it is invoked below of the alert function
singASong()
console.log('clap clap clap')


// Prompt is a Built-in javascript function that we can use to take input from the user
function enterUserName(){
	let userName = prompt('Enter your username')
	console.log(userName)
}

enterUserName()

// you can use a prompt outside of a function. make sure to output the prompt value by assigning to a variable and using console.log
let userAge = prompt ('Enter your age')
console.log(userAge)